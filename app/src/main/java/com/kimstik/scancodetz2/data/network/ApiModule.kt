package com.kimstik.scancodetz2.data.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiModule {

    @GET("{ip}/")
    fun get(
        @Query("query") query : String,
        @Path("ip") ip : String
    ) : Call<String>
}
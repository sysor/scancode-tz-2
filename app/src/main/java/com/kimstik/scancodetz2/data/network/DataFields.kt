package com.kimstik.scancodetz2.data.network

import com.google.gson.annotations.SerializedName

data class DataFields(
    @SerializedName("fields")
    val answer : String
)

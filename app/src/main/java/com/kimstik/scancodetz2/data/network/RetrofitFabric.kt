package com.kimstik.scancodetz2.data.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitFabric {

    private const val BASE_URL : String = "http://"

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val api: ApiModule by lazy {
        retrofit.create(ApiModule::class.java)
    }
}
package com.kimstik.scancodetz2.data.repository

import com.kimstik.scancodetz2.data.network.RetrofitFabric
import com.kimstik.scancodetz2.util.HEX1
import com.kimstik.scancodetz2.util.HEX2
import retrofit2.Call

class DataRepository : DataRepositoryImlp {

    private val api = RetrofitFabric.api

    override fun get(formCount: String,serialNumber: String, formFields: String, ip: String): Call<String> {
        val finalQuery = "Q1$formCount$HEX1$serialNumber$HEX2$formFields\r"
        return api.get(finalQuery, ip)
    }
}
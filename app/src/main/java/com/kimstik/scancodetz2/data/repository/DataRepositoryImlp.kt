package com.kimstik.scancodetz2.data.repository

import com.kimstik.scancodetz2.data.network.RetrofitFabric.api
import retrofit2.Call

interface DataRepositoryImlp {

    fun get(formCount: String,serialNumber: String, formFields: String, ip: String): Call<String>
}
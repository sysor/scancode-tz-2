package com.kimstik.scancodetz2.presenters

import com.arellomobile.mvp.MvpPresenter
import com.kimstik.scancodetz2.data.repository.DataRepository
import com.kimstik.scancodetz2.presenters.view.SendDataView
import com.kimstik.scancodetz2.util.fieldFromFields
import com.kimstik.scancodetz2.util.fieldsFromQuery
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendDataPresenter : MvpPresenter<SendDataView>() {

    private val apiQuery = DataRepository()

    fun getQuery(formCount: String,serialNumber: String, formFields: String, ip: String){

        apiQuery.get(formCount, serialNumber, formFields, ip).enqueue(object: Callback<String>{
            override fun onResponse(call: Call<String>, response: Response<String>) {
                viewState.setData(getListFields(response.body()!!))
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getListFields(answer: String) : List<String>{

        return answer.fieldsFromQuery()[1].fieldFromFields()
    }
}
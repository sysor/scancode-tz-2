package com.kimstik.scancodetz2.presenters.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface SendDataView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setData(data: List<String>)

    @StateStrategyType(SkipStrategy::class)
    fun queryError()

}
package com.kimstik.scancodetz2.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.kimstik.scancodetz2.databinding.ActivityMainBinding
import com.kimstik.scancodetz2.presenters.MainPresenter
import com.kimstik.scancodetz2.presenters.view.MainView
import com.kimstik.scancodetz2.util.*

class MainActivity : MvpActivity(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreferences = getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)

        // Если данные заполнены переходим к следующей Activity
        if (sharedPreferences.getBoolean(FLAG, false)) {
            nextActivity()
        }

        //Проверяем заполнены ли данные, если да, то сохраняем и вызываев следующую Activity
        binding.btnNext.setOnClickListener {
            if (checkEditText()) {
                setData()
            } else {
                Toast.makeText(this, "Заполните поля", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun nextActivity() { //Переходим ко 2 Activity
        val intent = Intent(this, SendDataActivity::class.java)
        startActivity(intent)
    }

    private fun setData() { //Сохраняем данные в sharedPreference

        val list: ArrayList<String> = ArrayList() //Получаем IP
        list.add(binding.etIP1.text.toString())
        list.add(binding.etIP2.text.toString())
        list.add(binding.etIP3.text.toString())
        list.add(binding.etIP4.text.toString())

        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.apply {
            putInt(PORT, binding.etPort.text.toString().toInt())
            putString(IP, list.ipToString())
            putInt(TIMEOUT, binding.etTimeout.text.toString().toInt())
            putBoolean(FLAG, true)
        }
        editor.apply()

        nextActivity() //После сохранения переходим к следующей Activity
    }

    private fun checkEditText(): Boolean { // Спагетти TODO испровить
        //Проверяем все ли EditText заполнены
        return if (binding.etTimeout.text.toString() != "") {

            if (binding.etPort.text.toString() != "") {

                if (binding.etIP1.text.toString() != "") {

                    if (binding.etIP2.text.toString() != "") {

                        if (binding.etIP3.text.toString() != "") {

                            binding.etIP4.text.toString() != ""
                        } else {
                            false
                        }
                    } else {
                        false
                    }
                } else {
                    false
                }
            } else {
                false
            }
        } else {
            false
        }
    }
}
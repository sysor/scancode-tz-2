package com.kimstik.scancodetz2.ui

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.kimstik.scancodetz2.R
import com.kimstik.scancodetz2.databinding.ActivitySendDataBinding
import com.kimstik.scancodetz2.presenters.SendDataPresenter
import com.kimstik.scancodetz2.presenters.view.SendDataView
import com.kimstik.scancodetz2.util.DELIMITER
import com.kimstik.scancodetz2.util.IP
import com.kimstik.scancodetz2.util.PORT
import com.kimstik.scancodetz2.util.toEditable


class SendDataActivity : MvpActivity(), SendDataView {

    @InjectPresenter
    lateinit var presenter: SendDataPresenter

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var binding: ActivitySendDataBinding

    private lateinit var editTexts: ArrayList<EditText> //Список Edit'текстов

    private var ip: String = ""
    private var port: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySendDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ip = sharedPreferences.getString(IP, "127.0.0.1")!! // Сохраняем ip
        port = sharedPreferences.getInt(PORT, 3000) // Получаем порт

        editTexts.add(binding.etField1)
        editTexts.add(binding.etField2)
        editTexts.add(binding.etField3)
        editTexts.add(binding.etField4)
        editTexts.add(binding.etField5)
        editTexts.add(binding.etField6)
        editTexts.add(binding.etField7)
        editTexts.add(binding.etField8)

        for (i in 0 until editTexts.size) {
            editTexts[i].setOnFocusChangeListener { v, hasFocus -> setOnFocusChangeListener(v, hasFocus) }
        }
    }

    override fun setData(data: List<String>) { // Сохраняем данные в fields
        for (i in 0 until editTexts.size) {
            editTexts[i].text = data[i].toEditable()
        }
    }

    override fun queryError() { // Выдаем ошибку если интернет запрос не прошел
        Toast.makeText(this, "Что-то пошло не так", Toast.LENGTH_LONG).show()
    }

    private fun setOnFocusChangeListener(v: View, hasFocus: Boolean) { // Слушатель Edit'текстов

        if (!hasFocus) { // Если фокус перешел на следующий Edit'текст отправляем query запрос
            when (v.id) {
                R.id.etField8 -> if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    binding.etField1.focusable = View.FOCUSABLE
                }
                else -> Unit
            }

            presenter.getQuery(
                binding.etForm.text.toString(), // Индекс формы
                binding.etSerial.text.toString(),   // Серийный номер
                getEditTextFields(),    // Значение полей
                "$ip:$port"  // IP адрес + порт
            )
        }
    }

    private fun getEditTextFields(): String {
        var fields: String? = null // Переменная содержащая поля field 0-7

        for (i in 0 until editTexts.size) {  // Получаем содержимое fields

            fields += editTexts[i].text.toString()

            if (i != editTexts.size - 1) { //Все поля значения кроме последнего
                fields += DELIMITER
            }
        }
        return fields!! //Возвращем поля с разделитилем
    }
}
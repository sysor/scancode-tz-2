package com.kimstik.scancodetz2.util

import android.text.Editable


fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

fun List<String>.ipToString(): String = this.joinToString(separator = ".")

fun String.fieldsFromQuery(): List<String> = this.split(HEX2).map { it } // Разделяем ответ на 2 части (во 2-ой  поля)

fun String.fieldFromFields(): List<String> = this.split(DELIMITER) // Разделяем поля на элементы листа

const val PORT = "PORT"
const val SHARED_PREF = "SHARED_PREF"
const val IP = "IP"
const val TIMEOUT =  "TIMEOUT"
const val FLAG =  "FLAG"
const val HEX1  = "<0x01>"
const val HEX2  = "<0x02>"
const val DELIMITER  = "\t"
